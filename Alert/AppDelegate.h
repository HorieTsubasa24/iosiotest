//
//  AppDelegate.h
//  Alert
//
//  Created by HorieTsubasa on 2019/04/04.
//  Copyright © 2019 HorieTsubasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

