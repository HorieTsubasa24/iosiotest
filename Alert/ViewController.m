//
//  ViewController.m
//  Alert
//
//  Created by HorieTsubasa on 2019/04/04.
//  Copyright © 2019 HorieTsubasa. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self prepareAudio];
}

-(void)prepareAudio
{
    NSError *error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"alarm00" ofType:@"wav"];
    NSURL *url = [[NSURL alloc] initFileURLWithPath:path];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    
    if ( error != nil )
    {
        NSLog(@"Error %@", [error localizedDescription]);
    }
    
    [self.player prepareToPlay];
    [self.player setDelegate:NULL];
    NSTimeInterval ti = self.player.duration;
}

- (IBAction)ButtonAction:(id)sender {
    // 通知を作成
    UNMutableNotificationContent *unMutableNotice = [UNMutableNotificationContent new];
    // title、body、soundを設定
    unMutableNotice.title = @"おはようございます";
    unMutableNotice.body = @"今日も一日頑張ってください！";           // Unityで渡すのか調査
    unMutableNotice.sound = [UNNotificationSound defaultSound]; // 音声ファイル持ってくる
    
    // 通知タイミングを設定（今回は、実装後5秒後に通知を受信します）
    UNTimeIntervalNotificationTrigger *triger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:5 repeats:NO];
    
    // リクエストの作成
    UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:@"リクエスト" content:unMutableNotice trigger:triger];
    
    // NUserNotificationCenterにリクエストを投げる
    // アプリ起動中以外の状態で通知をおこなってくれる。
    [[UNUserNotificationCenter currentNotificationCenter] addNotificationRequest:request withCompletionHandler:nil];
    
    _Label.text = @"通知の作成";
    NSLog(@"通知の作成");
}

- (IBAction)PlaySound:(id)sender {
    [self.player play];
    
    _Label.text = @"音声の再生";
    NSLog(@"音声の再生");
}

@end
